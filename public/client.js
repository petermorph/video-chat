var videoChatLobby = document.getElementById("video-chat-lobby");
var videoChat = document.getElementById("video-chat");
var joinButton = document.getElementById("join");
var localVideo = document.getElementById("local-video");
var peerVideo = document.getElementById("peer-video");
var roomName = document.getElementById("room-name");

var constraints = {
  audio: true,
  video: {
    facingMode: "user",
    width: { ideal: 640, max: 1280 },
    height: { ideal: 360, max: 720 },
  }
};

async function getMedia(constraints) {
  let stream = null;

  try {
    stream = await navigator.mediaDevices.getUserMedia(constraints);
    localVideo.srcObject = stream;
    localVideo.onloadedmetadata = function (e) {
      localVideo.play();
    };
    console.log("Stream:", localVideo.srcObject);
  } catch (error) {

  }
}


joinButton.addEventListener("click", function () {
  if (roomName.value == "") {
    alert("Please enter a room name");
  } else {
    getMedia(constraints);
  }
});
